CocoonJS Extensions
=====

Documentation for these extensions [can be found here](http://doc.ludei.com)

You can find the final, minified files in the "build/" directory

How to generate the minified CocoonJS extensions
=====

If you wish to re-generate the final extensions file by yourself, you'll have to follow these instructions.

First install NodeJS (if you haven't installed it yet)

* OSX (with homebrew): `$ brew install node`
* Ubuntu: `$ apt-get install nodejs`
* For other platforms, [download from here](http://nodejs.org/download/)

Then you'll need to install Grunt's command line interface globally.

`$ npm install -g grunt-cli`

"cd" to the directory where this readme is.

Finally, run the following command

`$ grunt`

This will update the "build/" folder with the minified javascript extensions.

